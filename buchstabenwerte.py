from sympy import symbols, Eq, solve, And

# Buchstaben als Symbole
#                     0 1 2 3 4 5 6 7 8 9 10 11 12 13 14  15 16 17 18 19 20 21 22 23 24 25
#                     A B C D E F G H I J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z')
buchstaben = symbols('A B C D E F G H I J K L M N O P Q R S T U V W X Y Z')
print(buchstaben)


print(buchstaben[9] , buchstaben[20] , buchstaben[7] , buchstaben[20], 54)  # JUHU
print(buchstaben[4] , buchstaben[8] , buchstaben[13], 11)  # EIN
print(buchstaben[13] , buchstaben[4] , buchstaben[20] , buchstaben[4] , buchstaben[17], 22)  # NEUER
print(buchstaben[2] , buchstaben[0] , buchstaben[2] , buchstaben[7] , buchstaben[4], 55)  # CACHE
print(buchstaben[0] , buchstaben[20] , buchstaben[5], 12)  # AUF
print(buchstaben[1] , buchstaben[14] , buchstaben[17] , buchstaben[10] , buchstaben[20] , buchstaben[12], 79)  # BORKUM
print(buchstaben[3] , buchstaben[4] , buchstaben[17], 28)  # DER
print(buchstaben[20] , buchstaben[17] , buchstaben[11] , buchstaben[0] , buchstaben[20] , buchstaben[1], 46)  # URLAUB
print(buchstaben[5] , buchstaben[0] , buchstaben[4] , buchstaben[13] , buchstaben[6] , buchstaben[19], 49)  # FAENGT
print(buchstaben[6] , buchstaben[20] , buchstaben[19], 40)  # GUT
print(buchstaben[0] , buchstaben[13], 3)  # AN
print(buchstaben[0] , buchstaben[1] , buchstaben[4] , buchstaben[17], 33)  # ABER
print(buchstaben[22] , buchstaben[14], 43)  # WO
print(buchstaben[8] , buchstaben[18] , buchstaben[19], 40)  # IST
print(buchstaben[4] , buchstaben[17], 13)  # ER
print(buchstaben[21] , buchstaben[4] , buchstaben[17] , buchstaben[18] , buchstaben[19] , buchstaben[4] , buchstaben[2] ,
   buchstaben[10] , buchstaben[19], 106)  # VERSTECKT
print(buchstaben[3] , buchstaben[0] , buchstaben[25] , buchstaben[20], 25)  # DAZU
print(buchstaben[1] , buchstaben[11] , buchstaben[14] , buchstaben[18] , buchstaben[18], 97)  # BLOSS
print(buchstaben[5] , buchstaben[8] , buchstaben[23], 29)  # FIX
print(buchstaben[3] , buchstaben[0] , buchstaben[18], 38)  # DAS
print(buchstaben[17] , buchstaben[0] , buchstaben[4] , buchstaben[19] , buchstaben[18] , buchstaben[4] , buchstaben[11],
   59)  # RAETSEL
print(buchstaben[11] , buchstaben[14] , buchstaben[4] , buchstaben[18] , buchstaben[4] , buchstaben[13], 64)  # LOESEN
print(buchstaben[18] , buchstaben[14], 48)  # SO
print(buchstaben[11] , buchstaben[14] , buchstaben[18], 56)  # LOS
print(buchstaben[6] , buchstaben[4] , buchstaben[7] , buchstaben[19] , buchstaben[18], 86)  # GEHTS
print(buchstaben[7] , buchstaben[0] , buchstaben[15] , buchstaben[15] , buchstaben[24], 72)  # HAPPY
print(buchstaben[7] , buchstaben[20] , buchstaben[13] , buchstaben[19] , buchstaben[8] , buchstaben[13] , buchstaben[6],
   75)  # HUNTING

# Gleichungen erstellen
alle_gleichungen = [
    Eq(buchstaben[9] + buchstaben[20] + buchstaben[7] + buchstaben[20], 54),  # JUHU
    Eq(buchstaben[4] + buchstaben[8] + buchstaben[13], 11),  # EIN
    Eq(buchstaben[13] + buchstaben[4] + buchstaben[20] + buchstaben[4] + buchstaben[17], 22),  # NEUER
    Eq(buchstaben[2] + buchstaben[0] + buchstaben[2] + buchstaben[7] + buchstaben[4], 55),  # CACHE
    Eq(buchstaben[0] + buchstaben[20] + buchstaben[5], 12),  # AUF
    Eq(buchstaben[1] + buchstaben[14] + buchstaben[17] + buchstaben[10] + buchstaben[20] + buchstaben[12], 79),  # BORKUM
    Eq(buchstaben[3] + buchstaben[4] + buchstaben[17], 28),  # DER
    Eq(buchstaben[20] + buchstaben[17] + buchstaben[11] + buchstaben[0] + buchstaben[20] + buchstaben[1], 46),  # URLAUB
    Eq(buchstaben[5] + buchstaben[0] + buchstaben[4] + buchstaben[13] + buchstaben[6] + buchstaben[19], 49),  # FAENGT
    Eq(buchstaben[6] + buchstaben[20] + buchstaben[19], 40),  # GUT
    Eq(buchstaben[0] + buchstaben[13], 3),  # AN
    Eq(buchstaben[0] + buchstaben[1] + buchstaben[4] + buchstaben[17], 33),  # ABER
    Eq(buchstaben[22] + buchstaben[14], 43),  # WO
    Eq(buchstaben[8] + buchstaben[18] + buchstaben[19], 40),  # IST
    Eq(buchstaben[4] + buchstaben[17], 13),  # ER
    Eq(buchstaben[21] + buchstaben[4] + buchstaben[17] + buchstaben[18] + buchstaben[19] + buchstaben[4] + buchstaben[2] + buchstaben[10] + buchstaben[19], 106),  # VERSTECKT
    Eq(buchstaben[3] + buchstaben[0] + buchstaben[25] + buchstaben[20], 25),  # DAZU
    Eq(buchstaben[1] + buchstaben[11] + buchstaben[14] + buchstaben[18] + buchstaben[18], 97),  # BLOSS
    Eq(buchstaben[5] + buchstaben[8] + buchstaben[23], 29),  # FIX
    Eq(buchstaben[3] + buchstaben[0] + buchstaben[18], 38),  # DAS
    Eq(buchstaben[17] + buchstaben[0] + buchstaben[4] + buchstaben[19] + buchstaben[18] + buchstaben[4] + buchstaben[11], 59),  # RAETSEL
    Eq(buchstaben[11] + buchstaben[14] + buchstaben[4] + buchstaben[18] + buchstaben[4] + buchstaben[13], 64),  # LOESEN
    Eq(buchstaben[18] + buchstaben[14], 48),  # SO
    Eq(buchstaben[11] + buchstaben[14] + buchstaben[18], 56),  # LOS
    Eq(buchstaben[6] + buchstaben[4] + buchstaben[7] + buchstaben[19] + buchstaben[18], 86),  # GEHTS
    Eq(buchstaben[7] + buchstaben[0] + buchstaben[15] + buchstaben[15] + buchstaben[24], 72),  # HAPPY
    Eq(buchstaben[7] + buchstaben[20] + buchstaben[13] + buchstaben[19] + buchstaben[8] + buchstaben[13] + buchstaben[6], 75)  # HUNTING
]

print(alle_gleichungen)
alle_loesungen = solve(alle_gleichungen, buchstaben)
print(alle_loesungen)
if alle_loesungen:
    for buchstabe, wert in alle_loesungen.items():
        print(f"{buchstabe}: {wert}")




